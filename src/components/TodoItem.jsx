/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React from 'react'

import PropTypes from 'prop-types'

function TodoItem(props) {
  const {
    id,
    name,
    isComplete,
    onToggleTodo,
    onDeleteTodo,
  } = props

  return (
    <li>
      <span className="delete-item">
        <button type="submit" onClick={() => onDeleteTodo(id)}>
          X
        </button>
      </span>
      <input
        type="checkbox"
        checked={isComplete}
        onChange={() => onToggleTodo(id)}
      />
      {name}
    </li>
  )
}

/**
 * Typechecking props
 */
TodoItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  isComplete: PropTypes.bool.isRequired,
  onToggleTodo: PropTypes.func.isRequired,
  onDeleteTodo: PropTypes.func.isRequired,
}

export default TodoItem
