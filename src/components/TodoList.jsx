/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'

import TodoItem from './TodoItem'
import Loader from './Loader'

import { getTodos, updateTodo, destroyTodo } from '../lib/todoServices'

class TodoList extends Component {
  state = {
    todos: [],
    isLoading: true,
  }

  componentDidMount() {
    console.log('** TodoList componentDidMount')
    this.getTodos()
  }

  componentDidUpdate() {
    console.log('** TodoList componentDidUpdate')
  }

  getTodos = async() => {
    try {
      const data = await getTodos();
      this.setState({ todos: data })
    } catch (error) {
      console.log('TodoList -> getTodos -> error', error)
    } finally {
      this.setState({ isLoading: false })
    }
  }

  onToggleTodo = async (id) => {
    const { todos } = this.state
    const todo = todos.find(t => t.id === id)
    const toggled = { ...todo, isComplete: !todo.isComplete }

    try {
      await updateTodo(toggled)

      // UPDATE
      await this.getTodos()
    } catch (error) {
      console.log('TodoList -> onToggleTodo -> error', error)
    }
  }

  onDeleteTodo = async (id) => {
    try {
      await destroyTodo(id)

      // UPDATE
      await this.getTodos()
    } catch (error) {
      console.log('TodoList -> onToggleTodo -> error', error)
    }
  }

  getVisibleTodos = (filter) => {
    const { todos = [] } = this.state
    switch (filter) {
      case 'active':
        return todos.filter(t => !t.isComplete)
      case 'completed':
        return todos.filter(t => t.isComplete)
      default:
        return todos
    }
  }

  render() {
    const { isLoading } = this.state
    const { filter } = this.props

    return (
      <div className="Todo-List">
        {
          isLoading
            ? <Loader isLoading />
            : (
                <ul>
                  {
                    this.getVisibleTodos(filter).map(todo => (
                      <TodoItem
                        key={todo.id}
                        onToggleTodo={this.onToggleTodo}
                        onDeleteTodo={this.onDeleteTodo}
                        {...todo}
                      />
                    ))
                  }
                </ul>
              )
        }

      </div>
    )
  }
}


TodoList.defaultProps = {
  todos: [],
}

/**
 * Typechecking props
 */
TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape(
      {
        id: PropTypes.number,
        name: PropTypes.string,
        isComplete: PropTypes.bool,
      },
    ),
  ),
}


export default TodoList
