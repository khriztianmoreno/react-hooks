/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React, { Component } from 'react'

import { createTodo } from '../lib/todoServices'

class TodoForm extends Component {
  state = {
    currentTodo: ''
  }

  componentDidUpdate() {
    console.log('** TodoForm componentDidUpdate')
  }

  handleInputChange = (evt) => {
    const val = evt.target.value
    this.setState({ currentTodo: val })
  }

  handleSubmit = (evt) => {
    evt.preventDefault()
    const { currentTodo } = this.state

    createTodo(currentTodo)
  }

  render() {
    const { currentTodo } = this.state
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          onChange={this.handleInputChange}
          value={currentTodo}
        />
      </form>
    )
  }
}

export default TodoForm
