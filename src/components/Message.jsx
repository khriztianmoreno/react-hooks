/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React from 'react'
import PropTypes from 'prop-types'

function Message({ message }) {
  if (!message) return null

  return <span className="message">{message}</span>
}

/**
 * Typechecking props
 */
Message.propTypes = {
  message: PropTypes.string,
}

Message.defaultProps = {
  message: null,
}

export default Message
