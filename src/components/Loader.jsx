/**
 * @author Cristian Moreno <khriztianmoreno@gmail.com>
 */

import React from 'react'
import PropTypes from 'prop-types'

import loading from '../loading.gif'

function Loader({ isLoading }) {
  return (
    <span className="loader">
      {isLoading ? <img src={loading} alt="loading content" /> : null}
    </span>
  )
}

/**
 * Typechecking props
 */
Loader.propTypes = {
  isLoading: PropTypes.bool,
}

Loader.defaultProps = {
  isLoading: false,
}

export default Loader
